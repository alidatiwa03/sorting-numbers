import React, { Component } from 'react';
import './App.css';


interface IProps{}
interface IState{
  tableau:number[]
  i:number
  item:number
}


class App extends Component<IProps, IState>{
  state={
    tableau:[],
    i:0,
    item:0
  }

  handleInputChange=(e:React.FormEvent<HTMLInputElement>)=>{
    let value= e.currentTarget.value
    this.setState({item:Number(value)})
  }

  displayNumber=()=>{
    return this.state.tableau.map(x=>x+', ')
  }

    ajouter = (e:React.FormEvent<HTMLInputElement>) =>{
      this.setState({tableau:[...this.state.tableau, this.state.item], item:0})

    }

    supprimer = ()=>{
      let tab:number[]=[]
      for (let i:number = 0; i < this.state.tableau.length; i++) {
        if(this.state.tableau[i]!==this.state.item){
          tab.push(this.state.tableau[i])
        }
      }  
      return this.setState({tableau:tab})
    }

    trie = () =>{
     let p=0
      for (let i = 0; i < this.state.tableau.length; i++) {
         for (let j = i+1; j < this.state.tableau.length; j++) {
           if(this.state.tableau[i]>this.state.tableau[j]) {
             let theTab:number[] = this.state.tableau
             p=theTab[i]
             theTab[i] = theTab[j]
             theTab[j]=p
             this.setState({tableau:theTab})
           }
           
         }
        
      }
      this.setState({tableau:[...this.state.tableau.map(x =>x),]})
    }

    textInput = React.createRef();

  render(){
    return (
      <div>
        <input type="text" name="inp" onChange={this.handleInputChange} value={this.state.item===0?'':this.state.item} id="in"></input><br></br>
        <input type="button" onClick={(e)=>this.ajouter(e)} value="ajouter"></input>
        <input type="button" onClick={this.trie} value="trier"></input>
        <input type="button" onClick={this.supprimer} value="supprimer"></input>
        
        <p>{this.displayNumber()}</p>
      </div>
    );
  }
}

export default App;


